using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMana : MonoBehaviour
{
    public Object GameCoin;
    private GameObject[] _coins;



    void Start()
    {
        GenerateEnemies(20);

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void RemoveEnemy(GameObject go)
    {
        for (int i = 0; i < _coins.Length; i++)
        {
            if (_coins[i] == go)
            {
                _coins[i] = null;
            }
        }
    }
    private void GenerateEnemies(int num)
    {

        _coins = new GameObject[num];
        for (int i = 0; i < num; i++)
        {
            GameObject go = GameObject.Instantiate(GameCoin) as GameObject;
            Vector3 vdir = new Vector3(Random.Range(-100.0f,-30.0f), Random.Range(.8f, .8f), Random.Range(-120.0f, -22.0f));
            //if (vdir.magnitude < 0.001f)
            //{
            //    vdir.x = 1.0f;
            //}
            //vdir.Normalize();
            go.transform.position = vdir * Random.Range(.8f, .8f);
            _coins[i] = go;
        }
    }




}
