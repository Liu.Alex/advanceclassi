using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    public Object DestoryEffect;


    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        Debug.Log("Enter");
        GameObject gEffect = Instantiate(DestoryEffect) as GameObject;
        gEffect.transform.position = transform.position;

    }
}


